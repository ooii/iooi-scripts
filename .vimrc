"set runtimepath=/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after
set runtimepath=/home/ionaic/.vim,/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after,/home/ionaic/.vim/after

syntax on
set number
set hlsearch
set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set directory=~/.vim/swap
set backspace=2

"set smartindent "alternatively set autoindent
set cindent
set cinkeys-=0#
set indentkeys-=0#
set spelllang=en_us
"set spell

inoremap ;; <Esc>
inoremap jj <Esc>

" GLSL syntax
au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.fs,*.vs,*.glsl setlocal ft=glsl

" Cg syntax
au BufNewFile,BufRead *.cg setf cg

" Arduino syntax
autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
