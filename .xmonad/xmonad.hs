-- Ian Ooi's xmonad config

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run (spawnPipe)
import System.IO
import XMonad.Util.EZConfig(additionalKeys)
import Data.Map (fromList)
import Data.Monoid (mappend)

manageHookSteam = composeAll 
    [ className =? "Steam" --> doFloat
    , className =? "glfw" --> doFloat
    ]

main = spawnPipe "/usr/bin/xmobar /home/ionaic/.xmobarrc" >>= \xmproc ->
    xmonad $ defaultConfig
    { terminal = "xterm"--"terminal"
    , manageHook = manageDocks <+> manageHook defaultConfig
    , layoutHook = avoidStruts $ layoutHook defaultConfig
    , logHook = dynamicLogWithPP xmobarPP
                    { ppOutput = hPutStrLn xmproc
                    , ppTitle = xmobarColor "green" "" . shorten 50
                    }
    -- Rebind mod to windows key
    -- ,modMask = mod4Mask
    -- border color/width
    --, borderWidth = 2
    , normalBorderColor = "#0a06d0" --darker "Cu(III)SO4 . 5H2O blue
    --, normalBorderColor = "#1f0fdf" --medium dark blue, intense
    --, focusedBorderColor = "#aaaaff" --very light grey-blue
    --, focusedBorderColor = "#b87333" --copper color
    , focusedBorderColor = "#da8a67" --lighter, crayola copper
    --, focusedBorderColor = "#cb6d51" --copper red
    } `additionalKeys` -- volume control keys
        [ ((0 , 0x1008FF11), spawn "amixer -c 0 set Master 2- unmute"), -- lower volume
          ((0,  0x1008FF12), spawn "amixer set Master toggle"), -- mute
          ((0, 0x1008FF13), spawn "amixer -c 0 set Master 2+ unmute") --raise volume
        ]
